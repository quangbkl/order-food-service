'use strict';

const products = require('../controllers/products');
const orders = require('../controllers/orders');

module.exports = function (app) {
    app.route('/products')
        .get(products.reads)
        .post(products.create);

    app.route('/products/:_id')
        .get(products.read)
        .put(products.update)
        .delete(products.delete);

    app.route('/orders')
        .get(orders.reads)
        .post(orders.create);

    app.route('/orders/:_id')
        .get(orders.read)
        .put(orders.update)
        .delete(orders.delete);

    app.route('/top_selling')
        .get(orders.topSelling);
};
