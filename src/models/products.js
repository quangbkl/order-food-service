'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Products = new Schema({
    title: {
        type: String,
        required: 'Enter the title of product.'
    },
    description: {
        type: String,
        default: ''
    },
    price: {
        type: Number,
        required: 'Enter the price of product.'
    },
    pictures: {
        type: Array,
        default: []
    },
    address: {
        type: String,
        required: 'Enter the address of product.'
    },
    created: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('products', Products);