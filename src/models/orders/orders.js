'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Orders = new Schema({
    product: {
        type: Schema.ObjectId,
        required: 'Please enter the product id of the order.'
    },
    quantity: {
        type: Number,
        required: 'Please enter the quantity of the order.'
    },
    type: {
        type: String,
        // 'reservations', 'shipping'
        required: 'Please enter the type of the order.'
    },
    pricing_summary: {
        subtotal: {
            type: Number,
            required: 'Field subtotal is not null.'
        },
        shipping: {
            type: Number,
            default: 0
        },
        tax: {
            type: Number,
            default: 0
        },
        total: {
            type: Number,
            required: 'Field total is not null.'
        }
    },
    ship_to: {
        contact_address: {
            address: {
                type: String,
                required: 'Field address is not null.'
            },
            address2: {
                type: String,
                default: null
            },
            city: {
                type: String,
                default: null
            },
            province: {
                type: String,
                default: null
            },
            zip_code: {
                type: String,
                default: null
            },
            country: {
                type: String,
                default: null
            }
        },
        first_name: {
            type: String,
            required: 'Field first_name is not null.'
        },
        last_name: {
            type: String,
            required: 'Field last_name is not null.'
        },
        company: {
            type: String,
            default: null
        },
        phone_number: {
            type: String,
            default: null
        },
        email: {
            type: String,
            default: null
        }
    },
    status: {
        type: String,
        // 'processing', 'shipped', 'cancelled'
        default: 'processing'
    },
    created: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('orders', Orders);