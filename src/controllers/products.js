'use strict';

const mongoose = require('mongoose');
const Products = mongoose.model('products');

const _parseSearch = (text) => {
    if (!text) return null;

    const words = text.split(' ');
    const regexText = words.map(word => {
        return {$regex: word, $options: "i"};
    });

    return regexText;
}

const _makeQuerySearch = (regexText) => {
    if (!regexText) return {};

    const query = {
        $or: [
            ...regexText.map(regexItem => {
                return {title: regexItem}
            }),
            ...regexText.map(regexItem => {
                return {description: regexItem}
            })
        ]
    }

    return query;
}

exports.reads = async (req, res) => {
    try {
        const {page: pageStr, limit: limitStr, search} = req.query;
        let page = !!pageStr ? parseInt(pageStr) : 1;
        let limit = !!limitStr ? parseInt(limitStr) : 10;

        if (page <= 0) page = 1;
        if (limit < 0) limit = 10;

        const regexSearch = _parseSearch(search);
        const query = _makeQuerySearch(regexSearch);

        const products = await Products.find(query).sort({created: -1}).skip((page - 1) * limit).limit(limit);
        const total = await Products.find(query).countDocuments();

        const resProducts = products.map(product => {
            const {pictures} = product;
            const thumbnail = pictures[0] || '';

            return Object.assign({thumbnail}, product.toObject());
        });

        res.send({
            success: true,
            data: resProducts,
            page,
            limit,
            total
        });
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        });
    }
}

exports.create = async (req, res) => {
    try {
        const newProduct = Products(req.body);
        const product = await newProduct.save();

        res.send({
            success: true,
            data: product
        });
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        });
    }
}

exports.read = async (req, res) => {
    try {
        const product = await Products.findOne({_id: req.params._id});

        res.send({
            success: true,
            data: product
        });
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        });
    }
}

exports.update = async (req, res) => {
    try {
        const item = await Products.updateOne({_id: req.params._id}, req.body);
        res.send({
            success: true,
            data: item
        });
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        });
    }
}

exports.delete = async (req, res) => {
    try {
        const deleted = await Products.deleteMany({_id: req.params._id});
        res.send({
            success: true,
            data: deleted
        });
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        });
    }
}