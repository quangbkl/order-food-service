'use strict';

const mongoose = require('mongoose');
const Orders = mongoose.model('orders');
const Products = mongoose.model('products');

exports.create = async (req, res) => {
    try {
        const newItem = Orders(req.body);
        const item = await newItem.save();
        res.send({
            success: true,
            data: item
        });
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        });
    }
}

exports.reads = async (req, res) => {
    try {
        const {page: pageStr, limit: limitStr} = req.query;
        let page = !!pageStr ? parseInt(pageStr) : 1;
        let limit = !!limitStr ? parseInt(limitStr) : 10;

        if (page <= 0) page = 1;
        if (limit < 0) limit = 10;

        const query = {};

        const orders = await Orders.find(query).sort({created: -1}).skip((page - 1) * limit).limit(limit);
        const formattedOrders = await Promise.all(orders.map(async order => {
            const product = await Products.findOne({_id: order.product});
            const objProduct = !product ? {} : product.toObject();
            const result = Object.assign({}, order.toObject(), {product: objProduct});
            return result;
        }));
        const total = await Orders.find(query).countDocuments();

        res.send({
            success: true,
            data: formattedOrders,
            page,
            limit,
            total
        });
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        });
    }
}

exports.read = async (req, res) => {
    try {
        const item = await Orders.findOne({_id: req.params._id});
        const product = await Products.findOne({_id: item.product});

        const result = Object.assign({}, item.toObject(), {product: product.toObject()});

        res.send({
            success: true,
            data: result
        });
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        });
    }
}

exports.update = async (req, res) => {
    try {
        const item = await Orders.findOneAndUpdate({_id: req.params._id}, req.body);
        res.send({
            success: true,
            data: item
        });
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        });
    }
}

exports.delete = async (req, res) => {
    try {
        const deleted = await Orders.deleteMany({_id: req.params._id});
        res.send({
            success: true,
            data: deleted
        });
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        });
    }
}

const _parseTopSelling = (orders) => {
    const topSelling = {};

    for (let order of orders) {
        if (topSelling[order.product]) {
            topSelling[order.product]++;
        } else {
            topSelling[order.product] = 1;
        }
    }

    return topSelling;
}

const _formatTopSelling = async (topSelling) => {
    const result = [];

    const entries = Object.entries(topSelling);
    for (const [productId, count] of entries) {
        const product = await Products.findOne({_id: productId}, {__v: false}).lean();
        result.push(Object.assign(product, {number_of_orders: count}));
    }

    return result.sort((a, b) => b.number_of_orders - a.number_of_orders);
}

exports.topSelling = async (req, res) => {
    try {
        const orders = await Orders.find({}).lean();
        const topSelling = _parseTopSelling(orders);
        const formatted = await _formatTopSelling(topSelling);

        res.send({
            success: true,
            data: formatted
        });
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        });
    }
}