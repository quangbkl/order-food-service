'use strict';

const express = require('express');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 3000;
const mongoose = require('mongoose');
const model = require('./models/index');
const bodyParser = require('body-parser');
const routes = require('./routes/index');

mongoose.Promise = global.Promise;
// mongoose.connect('mongodb://localhost/orders_food', { useNewUrlParser: true });
mongoose.connect('mongodb+srv://order_food:q3rdOL47MjpiPb27@cluster0-oncyd.mongodb.net/test?retryWrites=true', { useNewUrlParser: true });

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

routes(app);

app.listen(port);

console.log('Server started on: ' + port);